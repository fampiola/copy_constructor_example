#include "PersonalInfoNoCopyConstr.h"

PersonalInfoNoCopyConstr::PersonalInfoNoCopyConstr()
{
	mName = "No Name";
	mAddress = "No Address";
	mPhone = new int(0);
}

PersonalInfoNoCopyConstr::PersonalInfoNoCopyConstr(string _name, string _address, int _phone)
{
	mName = _name;
	mAddress = _address;
	mPhone = new int(_phone);
}

PersonalInfoNoCopyConstr::~PersonalInfoNoCopyConstr()
{

}

void PersonalInfoNoCopyConstr::PrintAllElements()
{
	cout << "Name : " << mName << endl;
	cout << "Address : " << mAddress << endl;
	cout << "Phone : " << *mPhone << endl;
}

void PersonalInfoNoCopyConstr::ChangePhone(int _newPhone)
{
	*mPhone = _newPhone;
}
