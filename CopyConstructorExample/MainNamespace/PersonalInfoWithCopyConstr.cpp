#include "PersonalInfoWithCopyConstr.h"

PersonalInfoWithCopyConstr::PersonalInfoWithCopyConstr()
{
	mName = "No Name";
	mAddress = "No Address";
	mPhone = new int(0);
}

PersonalInfoWithCopyConstr::PersonalInfoWithCopyConstr(string _name, string _address, int _phone)
{
	mName = _name;
	mAddress = _address;
	mPhone = new int(_phone);
}

PersonalInfoWithCopyConstr::PersonalInfoWithCopyConstr(const PersonalInfoWithCopyConstr& _obj)
{
	this->mName = _obj.mName;
	this->mAddress = _obj.mAddress;
	// This operation of copy constructor reserves memory dynamically for mPhone of the new object
	// and stores into this memory the value of the mPhone of the original object (that is on the right of "=")
	this->mPhone = new int(*_obj.mPhone);
}

PersonalInfoWithCopyConstr::~PersonalInfoWithCopyConstr()
{

}

void PersonalInfoWithCopyConstr::PrintAllElements()
{
	cout << "Name : " << mName << endl;
	cout << "Address : " << mAddress << endl;
	cout << "Phone : " << *mPhone << endl;
}

void PersonalInfoWithCopyConstr::ChangePhone(int _newPhone)
{
	*mPhone = _newPhone;
}
