#ifndef PERSONALINFONOCOPYCONSTR_H_INCLUDED
#define PERSONALINFONOCOPYCONSTR_H_INCLUDED
#include <iostream>
using namespace std;

class PersonalInfoNoCopyConstr
{
	private:
		string mName;
		string mAddress;
		int* mPhone;

	public:
		PersonalInfoNoCopyConstr();
		PersonalInfoNoCopyConstr(string, string, int);
		~PersonalInfoNoCopyConstr();
		void PrintAllElements();
		void ChangePhone(int);
};

#endif