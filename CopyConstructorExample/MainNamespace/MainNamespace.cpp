#include <iostream>
#include "PersonalInfoNoCopyConstr.h"
#include "PersonalInfoWithCopyConstr.h"
using namespace std;

void ShallowCopyLocallyAnObject(PersonalInfoNoCopyConstr);
void NotFriend(PersonalInfoWithCopyConstr*);
void Friend(PersonalInfoWithCopyConstr*);

int main()
{
    cout << endl << "Shallow Copy Example" << endl;
    /*Shallow Copy of an object without any copy constructor: Example 1*/
    PersonalInfoNoCopyConstr Agis("Agis", "Nothomb", 1040);
    // CloneAgis is a copy from Agis object. Although without a copy constructor,
    // all pointer values are shared between CloneAgis and Agis objects
    PersonalInfoNoCopyConstr ShallowCopyAgis = Agis;
    ShallowCopyAgis.PrintAllElements();
    // Any change in the pointer values of one object affects the pointer values
    // of the other copied object
    Agis.ChangePhone(1000);
    ShallowCopyAgis.PrintAllElements();

    /*Shallow Copy of an object without any copy constructor: Example 2*/
    // This function copies Agis object locally only inside the function
    // Any change inside the function to the local object shall not affect Agis object
    ShallowCopyLocallyAnObject(Agis);
    // Without our copy constructor, they share the same address in pointers, so
    // any change inside the function affects the pointer values of the original object Agis
    Agis.PrintAllElements();

    cout << endl << "Deep Copy Example" << endl;
    /*Deep Copy of an object with the use of copy constructor: Example 3*/
    PersonalInfoWithCopyConstr Filio("Filio", "Nothomb", 3000);
    PersonalInfoWithCopyConstr DeepCopyAgis = Filio;
    DeepCopyAgis.PrintAllElements();
    Filio.ChangePhone(1000);
    DeepCopyAgis.PrintAllElements();
    Filio.PrintAllElements();

    /*No friend function, cannot access private members of class: Example 4*/
    NotFriend(&Filio);

    /*Friend function, can access even private members of class: Example 5*/
    Friend(&Filio);
}

void ShallowCopyLocallyAnObject(PersonalInfoNoCopyConstr _locallyShallowCopiedObject)
{
    _locallyShallowCopiedObject.PrintAllElements();
    _locallyShallowCopiedObject.ChangePhone(2000);
}

void NotFriend(PersonalInfoWithCopyConstr *_obj)
{
    // ERROR
    //_obj->mName = "New name";
}

void Friend(PersonalInfoWithCopyConstr* _obj)
{
   // Now i have access to private class members
    _obj->mName = "New name";
}
