#ifndef PERSONALINFOWITHCOPYCONSTR_H_INCLUDED
#define PERSONALINFOWITHCOPYCONSTR_H_INCLUDED
#include <iostream>
using namespace std;

class PersonalInfoWithCopyConstr
{
private:
	string mName;
	string mAddress;
	int* mPhone;

public:
	PersonalInfoWithCopyConstr();
	PersonalInfoWithCopyConstr(string, string, int);
	PersonalInfoWithCopyConstr(const PersonalInfoWithCopyConstr&);
	~PersonalInfoWithCopyConstr();
	void PrintAllElements();
	void ChangePhone(int);
	friend void Friend(PersonalInfoWithCopyConstr*);
};

#endif